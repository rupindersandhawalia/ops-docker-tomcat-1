#% description: Creates a Container VM with the provided Container manifest.
#% parameters:
#% - name: zone
#%   type: string
#%   description: Zone in which this VM will run
#%   required: true
#% - name: containerImage
#%   type: string
#%   description: Name of the Google Cloud Container VM Image
#%     (e.g., container-vm-v20150317).
#%   required: true
#% - name: containerManifest
#%   type: string
#%   description: String containing the Container Manifest in YAML
#%   required: true

"""Creates a Container VM with the provided Container manifest.
"""

import yaml


def GenerateConfig(context):
  # Loading the container manifest into a YAML object model so that it will be
  # serialized as a single JSON-like object when converted to string.
  manifest = yaml.load(context.imports[context.properties["containerManifest"]])

  return """
resources:
  - type: compute.v1.instance
    name: %(name)s
    properties:
      zone: %(zone)s
      machineType: https://www.googleapis.com/compute/v1/projects/%(project)s/zones/%(zone)s/machineTypes/n1-standard-1
      metadata:
        items:
          - key: google-container-manifest
            value: "%(manifest)s"
      disks:
        - deviceName: boot
          type: PERSISTENT
          boot: true
          autoDelete: true
          initializeParams:
            diskName: %(name)s-disk
            sourceImage: https://www.googleapis.com/compute/v1/projects/google-containers/global/images/%(containerImage)s
      networkInterfaces:
        - accessConfigs:
            - name: external-nat
              type: ONE_TO_ONE_NAT
          network: https://www.googleapis.com/compute/v1/projects/%(project)s/global/networks/default
""" % {"name": context.env["name"] + "-" + context.env["deployment"],
       "project": context.env["project"],
       "zone": context.properties["zone"],
       "containerImage": context.properties["containerImage"],
       "manifest": manifest}

