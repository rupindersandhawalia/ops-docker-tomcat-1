sapho-tomcat
===================
Based off of the official tomcat image 'https://registry.hub.docker.com/_/tomcat/'


Image tags
----------
```
    sapho/ops-docker-tomcat:latest
    sapho/ops-docker-tomcat:8-jre8
    sapho/ops-docker-tomcat:7-jre8
```

# How to use this image.

Please refer to the [Documentation](https://registry.hub.docker.com/_/tomcat/) from which this image is based on.

The only thing we do here is download our sapho server war file and place it into the webapps folder + set JVM memory settings.